## data prep fecal data CVID L6

setwd()

# read in data
data_L6 <- read.table("roos_fecal_silva_table-dada2-L6.tsv", sep = "\t", row.names=1, stringsAsFactors = FALSE)
feces_L6 <- as.data.frame(t(data_L6))
feces_L6.1 <-  data.frame(apply(droplevels(feces_L6),2,as.numeric),check.names = FALSE)

# Extract L6 names. If L6 is not provided, use higher order names
feces_L6_raw<-rename.OTU(feces_L6.1,"L6")
indx <- sapply(feces_L6_raw[-1,], is.factor)
feces_L6_raw[indx] <- lapply(feces_L6_raw[indx], function(x) as.numeric(as.character(x)))
feces_L6_raw$sampleID_feces<-feces_L6$sampleID

#head(feces_L6_raw)

## use mapping file defined in "data prep mf"
sample.1<-readRDS("sample.RDS")

## merge mapping file and sequencing data
merged<-merge(feces_L6_raw, sample.1, by='sampleID_feces')

## extract cleaned feces data
clean_L6<-merged[,3:268]
clean_L6$sampleID<-NULL
saveRDS(clean_L6, "clean_L6.RDS")

## throw out samples with low read count
feces_L6.2<- feces_L6_raw
feces_L6.2$sampleID<-NULL
row.names(feces_L6.2)<-feces_L6.2$sampleID_feces
feces_L6.2$sampleID_feces<-NULL
feces_L6.2$sum <- rowSums(feces_L6.2)
feces_L6.2$sampleID_feces <-row.names(feces_L6.2)
merged.1<-merge(feces_L6.2, sample.1, by="sampleID_feces", all=FALSE)

merged_1500 <- merged.1[!(merged.1$sum < 1500),]
row.names(merged_1500)<-merged_1500$sampleID_feces

## there are no samples with count below 1500 so none are thrown out.
merged_1500.1<-merged_1500
merged_1500.1$sampleID_feces<-NULL

saveRDS(merged_1500.1,"merged_L6_1500.RDS")

### split them back up
###Bacteria
L6_1500<-merged_1500.1[,1:266]
saveRDS(L6_1500,"L6_1500.RDS")
###sample
sample_1500<-merged_1500.1[,268:296]
saveRDS(sample_1500,"sample_1500.RDS")


###############################
#### compositional L6 data ####
###############################

OTU_comp_L6 <-(L6_1500) %>% comp 

saveRDS(OTU_comp_L6, "comp_L6.RDS")

#################################
#### CLR transformed L6 data ####
#################################
L6_CZM<-cmultRepl(L6_1500, method="CZM", output="prop")
# check: rowSums(L6_CZM)
L6_CLR<-clr(L6_CZM) %>% as.data.frame(sampleID=rownames(L6_1500), check.names=FALSE)

saveRDS(L6_CLR,"clr_L6.RDS")


######################################
#### Combine into Phyloseq object ####
######################################
library(phyloseq)

# Prepare data and combine it into phyloseq object
otu_L6 <- otu_table(t(L6_1500),taxa_are_rows = TRUE)
sample_phyl <- sample_data(sample_1500)

phyl_L6 <- phyloseq(otu_table(otu_L6),sample_data(sample_phyl))

saveRDS(phyl_L6, "phyloseq_L6.RDS")

