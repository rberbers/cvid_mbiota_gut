###############################
#### plot general overview ####
###############################
setwd()

L6_comp<-readRDS("comp_L6.RDS")

# sort by which genera are most abundant
median_L6 <- colMedians(as.matrix(L6_comp))
L6_comp.1<-rbind(L6_comp, median_L6)

L6_comp.2<-L6_comp.1[,order(-median_L6)]
# select top 10
L6_comp.3<-L6_comp.2[1:ncol(L6_comp),1:10]

### add the rest as 1 'other' category
L6_comp_rest<-L6_comp.2[1:ncol(L6_comp),11:ncol(L6_comp.2)]
L6_comp_rest$rowsum<-rowSums(L6_comp_rest)

L6_comp_all<-cbind(L6_comp.3,L6_comp_rest$rowsum)

## add sample data
L6_comp_all$sampleID<-rownames(L6_comp_all)

sample<-readRDS("sample_1500.RDS")
sample$sampleID<-row.names(sample)

L6_comp_sample<-merge(L6_comp_all, sample, by="sampleID")

HC<-subset(L6_comp_sample, imdys=='HC')
HC.1<-HC[,2:12]

XLA<-subset(L6_comp_sample, imdys=='XLA')
XLA.1<-XLA[,2:12]

CVID_IgA<-subset(L6_comp_sample, binary.0.1=='1')
CVID_IgA.1<-CVID_IgA[,2:12]

CVID_noIgA<-subset(L6_comp_sample, binary.0.1=='0')
CVID_noIgA.1<-CVID_noIgA[,2:12]

CVIDio<-subset(L6_comp_sample, imdys=='CVIDio')
CVIDio.1<-CVIDio[,2:12]

CVIDid<-subset(L6_comp_sample, imdys=='CVIDid')
CVIDid.1<-CVIDid[,2:12]

HC <- colMedians(as.matrix(HC.1))
XLA <- colMedians(as.matrix(XLA.1))
CVIDIgA <- colMedians(as.matrix(CVID_IgA.1))
CVIDnoIgA <- colMedians(as.matrix(CVID_noIgA.1))
CVIDio<- colMedians(as.matrix(CVIDio.1))
CVIDid<- colMedians(as.matrix(CVIDid.1))

### for imdys
comp_all <- rbind(HC, CVIDio, CVIDid, XLA) %>% as.data.frame()

comp_all$group<-row.names(comp_all)
## it needs to be in long format instead of wide
data_long <- gather(comp_all, bacterium, rel_abundance, Blautia:'L6_comp_rest$rowsum', factor_key=TRUE)
data_long

##### if you're unhappy about the order of the bars:
## redefine column 'group' as factor
data_long$group<-as.factor(data_long$group)
## check the order of the factors here
print(levels(data_long$group))
## reorder factors
data_long$group <- factor(data_long$group,levels(data_long$group)[c(3,2,1,4)])

## barplot

top10_L6<-ggplot(data = data_long, aes(x=group,y=rel_abundance,group=bacterium)) +
  geom_bar(aes(x=group, y=rel_abundance, fill=bacterium), stat="identity", position = position_stack(reverse = T)) +
  scale_fill_brewer(palette="Spectral") + scale_color_brewer(palette="Spectral") + ggtitle("top 10 most abundant genera")+
  theme(panel.background = element_blank())
top10_L6

ggarrange(top10_L6, ncol=1, nrow=1)%>% ggexport(filename="top10_L6_13april2021.pdf")
ggarrange(top10_L6, ncol=1, nrow=1)%>% ggexport(filename="top10_L6_13april2021.eps")
                                                  
